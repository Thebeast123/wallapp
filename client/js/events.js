Template.form.events({
    'submit form': function (event) {
        event.preventDefault();


        var imageFile = event.currentTarget.children[0].children[0].children[0].children[1].files[0];

        var name = event.currentTarget.children[1].value;

        var message = event.currentTarget.children[2].value;

        if (name == "") {
            Materialize.toast('dont submit any thing empty', 4000) // 4000 is the duration of the toast
        }

        if (imageFile == undefined) {
            Materialize.toast('dont submit any thing empty', 4000) // 4000 is the duration of the toast
        }

        if (message == "") {
            Materialize.toast('dont submit any thing empty', 4000) // 4000 is the duration of the toast
        }


        Collections.Images.insert(imageFile, function (error, fileObject) {
            if (error) {

            } else {
                Collections.Post.insert({
                    name: name,
                    createdAt: new Date(),
                    message: message,
                    ImageID: fileObject._id,
                });
                $('.grid').masonry('reloadItems');
            }
        });
    }
});

Template.posts.events({
    'click .delete-post': function (event) {
        Collections.Post.remove({
            _id: this._id
        });
    }
});
Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
});